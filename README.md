# fail2ban-opnsense-alias-api-blocklist

Adding IPs banned by Fail2Ban to a host list alias on an OPNSense firewall via API for blocking at firewall level.

In System > Access > Users, create a new user with a username of your choice (I used banapi)
*   Required permissions are "Firewall: Alias: Edit" and "Firewall: Aliases"
*   Create an API key
*   Create a new alias Firewall > Aliases of type Host(s) with a name that makes sense (I used BANNED)
*   Create firewall rules that use the alias accordingly. I use a rule on WAN that blocks any IP in the BANNED alias from WAN for all ports. 


Copy opnsense-api-banalias.conf into /etc/fail2ban/action.d
Update opnsense-api-banalias.conf with the applicable information
*   API key
*   API secret
*   Alias name (ex BANNED)
*   Hostname of your OPNsense firewall


Update your fail2ban configs accordingly. I use this entry in jail.local with my Unifi NVR VM, so the config for that looks like this:

```
[unifivideo-failed]

enabled         = true

port            = 7443
filter          = unifivideo-failed
logpath         = /var/log/unifi-video/login.log
maxretry        = 5
findtime        = 600
bantime         = -1
banaction       = opnsense-api-banalias
```


Note that I currently leave IPs banned forever so I dont have an unban action.


**Important consideration for testing and the application you are using this with:**
Opnsense is based on pf which only evaluates rules at the start of a connection.
What this means can be described in practical terms as this, which I found with my Unifi NVR:
*   Attacker accesses the UI, enters a password incorrectly
*   Fail2Ban tracks that attempt
*   Attacker tries password2, password3, password4, password5
*   Fail2Ban bans the IP for five attempts
*   Attacker can continue attacking on the same session because pf is still allowing the connection as it is active and does not reevaluate
*   If attacker closes the connection and tries a new one from the same IP they are now blocked

        
The outcome of the above is that there needs to be another method where the connection is closed. 
Once it is closed the firewall rule will come into play on the next connection attempt.

I accomplish this with a HAProxy rule which will disconnect a connection after a certain number of hits to a URI.

This HAProxy rule would be very dependant on the application/etc.